# api-serverless

Ejemplo bascio de una api serverless deployada sobre https://vercel.com/dashboard


## Qué es ServerLess

Aunque significa "sin servidor" esto no es así, lo que ocurre es que dejas de usar un servidor físico o uno en la nube claramente identificados por unos contenedores temporales y sin estado donde se ejecutan los códigos de las aplicaciones. Estos contenedores se crean en el momento que ejecutas la aplicación y luego desaparecen, por lo que el servidor pasa a ser una parte menos visible del sistema.

Esta tecnología se asocia con la arquitectura FaaS (Functions as a Server) que proporcionan una infraestructura autogestionada, que nos permite ejecutar directamente nuestro código reaccionando a eventos, estos eventos pueden ser tanto eventos web como eventos provenientes de otras partes de la infraestructura.

## Configuracion en vercel

En este ejemplo se usa el siguiente archivo de configuracion, el cual es necesario para los deploy en vercel.

```
{
    "version": 2,
    "name": "api-serverless",
    "builds": [
        {
            "src": "index.js",
            "use": "@now/node-server"
        }
    ],
    "routes": [
        {
            "src": "^/(.*)",
            "dest": "/index.js",
            "methods": [
                "GET", "POST"
            ]
        }
    ]
}
```

En la parte de rutas, se configuran los diferentes metodos para acceder a ls funciones de nuestro proyecto, en este caso le decimos que acceda al las rutas configuradas a partir del index.js. 

### Fuentes

- https://vercel.com/docs/configuration#introduction/configuration-reference
